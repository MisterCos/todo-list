describe('Form filling and submission', () => {
  beforeEach(() => {
    cy.viewport('macbook-15')
    cy.visit('http://localhost:4200/');

  });
  it('Fills the form and submits', () => {
    const testName = 'cypress test';
    const testDescription = 'Test Description';


    // Rellena el campo de entrada "name"
    cy.get('input[name="name"]').type(testName);

    // Rellena el campo de entrada "description"
    cy.get('input[name="description"]').type(testDescription);

    // Hace clic en el botón "save"
    cy.get('button[name="save"]').click();

    // Verifica que los datos se han guardado en la tabla
    cy.get('table').contains('td', testName);
    cy.get('table').contains('td', testDescription);

    cy.contains('created successfully')
  });

  it('input should show error message', () => {
    cy.get('button[name="save"]').click();
    cy.get('input[name="name"]').should('have.class', 'ng-invalid');
    cy.contains('Name is required')
    cy.get('input[name="description"]').should('have.class', 'ng-invalid');
    cy.contains('Description is required')
  })

  it('Fills the form and reset it ', () => {
    const testName = 'Test Name';
    const testDescription = 'Test Description';

    cy.visit('http://localhost:4200/');

    // Rellena el campo de entrada "name"
    cy.get('input[name="name"]').type(testName);

    // Rellena el campo de entrada "description"
    cy.get('input[name="description"]').type(testDescription);

    // Hace clic en el botón "save"
    cy.get('button[name="discard"]').click();

    cy.get('input[name="name"]').should('have.value', '');
    cy.get('input[name="description"]').should('have.value', '');

  });

  it('table item switch', () => {
    const tablerow = cy.get('table').contains('tr', 'cypress test');
    tablerow.find('p-inputSwitch').click();
    console.log(tablerow);
    cy.wait(1000);
    cy.contains('updated successfully')
  });

  it('delete item in table', () => {
    const tablerow = cy.get('table').contains('tr', 'cypress test');
    tablerow.trigger('hover');

    tablerow.find('button[name="deleteInline"]').invoke('addClass','block').click();
    cy.get('table').contains('tr', 'cypress test').should('not.exist');
    cy.contains('deleted successfully')
  })
});
