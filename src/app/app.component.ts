import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { Task } from './models/Task';
import { TaskService } from './services/task.service';
import { TasksTableComponent } from './tasks-table/tasks-table.component';
const primengModules = [CardModule, ToastModule];

@Component({
  selector: 'app-root',
  standalone: true,

  imports: [
    TasksTableComponent,
    RouterOutlet,
    ...primengModules,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [TaskService, MessageService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  tasks: Task[] = [];

  constructor(
    private taskService: TaskService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.getAllTask();
  }

  getAllTask() {
    this.taskService.getALlTasks().subscribe((tasks) => {
      this.tasks = tasks;
      this.orderTask();
    });
  }

  orderTask() {
    this.tasks.sort((a, b) => Number(a.completed) - Number(b.completed));
    console.log(this.tasks);
  }

  editTask(task: Task) {
    this.taskService.updateTask(task).subscribe((task) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Success',
        detail: `Task with id ${task.id} updated successfully`,
      });
      this.orderTask();
    });
  }

  // as we dont pass the id of the task to be created, we will use the Partial type to create a new task
  createTask(task: Partial<Task>) {
    this.taskService.createTask(task).subscribe((task: Task) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Success',
        detail: `Task with id ${task.id} created successfully`,
      });
      this.getAllTask();
    });
  }

  deleteTask(taskId: string) {
    this.taskService.deleteTask(taskId).subscribe((task: Task) => {
      this.messageService.add({
        severity: 'success',
        summary: 'Success',
        detail: ` ${task.name} deleted successfully`,
      });
      this.getAllTask();
    });
  }

  filterTask(event: any) {
    this.taskService.getALlTasks().subscribe((tasks) => {
      if (event === 'ALL') {
        this.tasks = tasks;
      }
      if (event === 'Pending') {
        this.tasks = tasks.filter((task) => !task.completed);
      }
      if (event === 'Completed') {
        this.tasks = tasks.filter((task) => task.completed);
      }
      this.orderTask();
    });
  }

  get remainingTasks() {
    return this.tasks.filter((task) => !task.completed).length;
  }
}
