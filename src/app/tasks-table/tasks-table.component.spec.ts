import { ComponentFixture, TestBed } from '@angular/core/testing';
import { expect } from '@jest/globals';
import { TasksTableComponent } from './tasks-table.component';
import { ReactiveFormsModule, FormsModule, FormControl } from '@angular/forms';

describe('TasksTableComponent', () => {

  const taskMock = {name: 'Test Task', description: 'Test Description', completed: false}
  const taskMockInvalid = {name: '', description:'', completed: false}

  let component: TasksTableComponent;
  let fixture: ComponentFixture<TasksTableComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TasksTableComponent,ReactiveFormsModule, FormsModule]
    })
    .compileComponents();
    fixture = TestBed.createComponent(TasksTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit createTaskEvent when createTask is called with valid form', () => {
    jest.spyOn(component.createTaskEvent, 'emit');
    component.taskForm.setValue(taskMock);
    component.createTask();
    expect(component.createTaskEvent.emit).toHaveBeenCalledWith(taskMock);
  });
  it('should not emit createTaskEvent when createTask is called with invalid form', () => {
    jest.spyOn(component.createTaskEvent, 'emit');
    component.taskForm.setValue(taskMockInvalid);
    component.createTask();
    expect(component.createTaskEvent.emit).not.toHaveBeenCalled();
  });
  it('should reset the form when discardCreateTask is called', () => {
    component.taskForm.setValue(taskMock);
    component.discardCreateTask();
    expect(component.taskForm.value).toEqual({name: null, description: null, completed: null});
  });

  it('should emit filterTaskEvent when filterTask is called', () => {
    jest.spyOn(component.filterTaskEvent, 'emit');
    component.filterTask({value: {code: 'Test Code'}});
    expect(component.filterTaskEvent.emit).toHaveBeenCalledWith('Test Code');
  });

  it('should emit editTaskEvent when editTask is called', () => {
    const task = {id: '1', name: 'Test Task', description: 'Test Description', completed: false};
    jest.spyOn(component.editTaskEvent, 'emit');
    component.editTask(task);
    expect(component.editTaskEvent.emit).toHaveBeenCalledWith(task);
  });

  it('should return true if formControlName has no errors', () => {
    const formControlName = new FormControl({ errors: null });
    const result = component.isRequiredField(formControlName);
    expect(result).toBe(true);
  });
});
