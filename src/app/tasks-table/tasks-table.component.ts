import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { FilterConstant } from '../constants/FilterConstant';
import { Task } from '../models/Task';

const primengModules = [
  CardModule,
  TableModule,
  ToastModule,
  DropdownModule,
  ButtonModule,
  InputTextModule,
  InputSwitchModule,
  ToolbarModule,
];

@Component({
  selector: 'app-tasks-table',
  standalone: true,
  imports: [...primengModules, ReactiveFormsModule, FormsModule],
  templateUrl: './tasks-table.component.html',
  styleUrl: './tasks-table.component.scss',
})
export class TasksTableComponent {
  @Input() tasks: Task[] = [];
  @Output() createTaskEvent = new EventEmitter<Task>();
  @Output() deleteTaskEvent = new EventEmitter<string>();
  @Output() editTaskEvent = new EventEmitter<Task>();
  @Output() filterTaskEvent = new EventEmitter<Task>();

  isSubmitted = false;
  filterOptions = FilterConstant;
  selectedOption = this.filterOptions[0]
  taskForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    completed: new FormControl(false),
  });

  createTask() {
    this.isSubmitted = true;
    if (this.taskForm.valid) {
      this.createTaskEvent.emit(this.taskForm.value);
      this.taskForm.reset();
      this.isSubmitted = false;
    }
  }

  discardCreateTask() {

    this.taskForm.reset();
  }

  filterTask(event:any) {
    const {code}  = event.value;
    this.filterTaskEvent.emit(code);
  }
  editTask(task:Task) {
    this.editTaskEvent.emit(task);
  }

  deleteTask(id:string) {
    this.deleteTaskEvent.emit(id);
  }


  isRequiredField(formControlName: AbstractControl) {
    if (!formControlName.errors) {
      return true;
    }

    if (formControlName.errors['required']) {
      return true;
    }
    return false;
  }

  get tasksForm() {
    return this.taskForm.controls;
  }
}
