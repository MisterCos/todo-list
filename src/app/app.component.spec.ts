import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TaskService } from './services/task.service';
import { MessageService } from 'primeng/api';
import { of } from 'rxjs';
import { Task } from './models/Task';
import { expect } from '@jest/globals';
describe('AppComponent', () => {
  const tasksMock = [
    { id: '1', name: 'Test Task 1', description: 'test', completed: true },
    { id: '2', name: 'Test Task 2', description: 'test', completed: false },
  ];

  let component: AppComponent;
  let taskService: TaskService;
  let messageService: MessageService;
  const taskServiceMock = {
    getALlTasks: jest.fn(() => of([])),
    updateTask: jest.fn(() => of({})),
    createTask: jest.fn(() => of(tasksMock[0])),
  };

  const messageServiceMock = {
    add: jest.fn(),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppComponent],
      providers: [
        { provide: TaskService, useValue: taskServiceMock },
        { provide: MessageService, useValue: messageServiceMock },
      ],
    }).compileComponents();

    taskService = TestBed.inject(TaskService);
    messageService = TestBed.inject(MessageService);
    const fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });
  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should get all tasks on init', () => {
    const spy = jest.spyOn(component, 'getAllTask');
    const filterSyp = jest.spyOn(component, 'orderTask');
    component.ngOnInit();

    expect(spy).toHaveBeenCalled();

  });

  it('should order tasks', () => {
    component.tasks = tasksMock;
    component.orderTask();
    expect(component.tasks[0].completed).toBe(false);
    expect(component.tasks[1].completed).toBe(true);
  });

  it('should create a task', () => {
    const task: Partial<Task> = { name: 'Test Task 1', completed: false };
    component.createTask(task);
    taskService.createTask(task).subscribe(() => {
      expect(taskService.createTask).toHaveBeenCalledWith(task);
    });
  });
  it('should edit a task', () => {
    const task: Task = tasksMock[0];
    component.editTask(task);
    taskService.updateTask(task).subscribe(() => {
      expect(taskService.updateTask).toHaveBeenCalledWith(task);
    });
  });
});
