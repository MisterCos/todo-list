import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TaskService } from './task.service';
import { Task } from '../models/Task';
import { expect } from '@jest/globals';
describe('TaskService', () => {
  const taskMock:Task= { id: '1', name: 'Test Task 1',description:'test' ,completed: false }
  let service: TaskService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TaskService]
    });


    service = TestBed.inject(TaskService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch all tasks', () => {
    const dummyTasks: Task[] = [
      taskMock
    ];

    service.getALlTasks().subscribe(tasks => {
      expect(tasks.length).toBe(1);
      expect(tasks).toEqual(dummyTasks);
    });

    const req = httpMock.expectOne(`${service.baseUrl}/tasks`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyTasks);
  });

  it('should update a task', () => {
    const dummyTask: Task = taskMock;
    service.updateTask(dummyTask).subscribe(task => {
      expect(task).toEqual(dummyTask);
    });
    const req = httpMock.expectOne(`${service.baseUrl}/tasks/${dummyTask.id}`);
    expect(req.request.method).toBe('PUT');
    req.flush(dummyTask);
  });

  it('should create a task', () => {
    const dummyTask: Partial<Task> = { name: 'Test Task 1', completed: false };
    service.createTask(dummyTask).subscribe(task => {
      expect(task).toEqual(jasmine.objectContaining(dummyTask));
    });

    const req = httpMock.expectOne(`${service.baseUrl}/tasks`);
    expect(req.request.method).toBe('POST');
    req.flush(dummyTask);
  });

  it('should delete a task', () => {
    const dummyTask: Task = taskMock;
    service.deleteTask(dummyTask.id).subscribe(task => {
      expect(task).toEqual(dummyTask);
    });
    const req = httpMock.expectOne(`${service.baseUrl}/tasks/${dummyTask.id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(dummyTask);
  });
});
