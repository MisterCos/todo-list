import { Injectable } from '@angular/core';
import { environment } from '../../environment/environment.dev';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/Task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  baseUrl = environment.baseUrl;
  constructor(private httpClient: HttpClient) { }

  getALlTasks():Observable<Task[]> {
    return this.httpClient.get<Task[]>(`${this.baseUrl}/tasks`);
  }

  updateTask(task: Task):Observable<Task> {
    return this.httpClient.put<Task>(`${this.baseUrl}/tasks/${task.id}`, task);
  }

  createTask(task: Partial<Task>):Observable<Task> {
    return this.httpClient.post<Task>(`${this.baseUrl}/tasks`, task);
  }

  deleteTask(id: string):Observable<Task> {
    return this.httpClient.delete<Task>(`${this.baseUrl}/tasks/${id}`);
  }
}
