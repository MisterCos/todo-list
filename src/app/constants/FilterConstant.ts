export const FilterConstant = [
  { name: 'Show all', code: 'ALL' },
  { name: 'Show pending', code: 'Pending' },
  { name: 'Show completed', code: 'Completed' },
]
